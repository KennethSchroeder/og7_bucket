import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.nxt.LightSensor;
import lejos.nxt.Motor;
import lejos.nxt.SensorPort;

public class NeedForSpeed {
	private static String standart = "unknown";
	private static String isRight = "right";
	private static String isLeft = "left";
	private static String direction = standart;
	static LightSensor sensor = new LightSensor(SensorPort.S1);
	
	public static void main(String[] args) {
		
		Button.ENTER.waitForPress();
		
		Motor.A.forward();
		Motor.B.forward();
		
		do {
			Motor.A.setSpeed(300);					//setzt die Geschwindigkeit auf 300 zur�ck
			Motor.B.setSpeed(300);	
			Motor.A.forward();
			Motor.B.forward();
			if(sensor.getLightValue() > 44) {		//wenn er von der schwarzen linie abkommt
				if(direction == standart) {			//und nicht wei� in welche richtung er muss
					testDirection();
					
				}
				if(direction == isRight) {
					steerRight();
				}
				if(direction == isLeft) {
					steerLeft();
				}
				
				LCD.drawInt(sensor.getLightValue(), 1, 1);
				LCD.drawString( direction, 1, 2);
			}
			
		}while(!Button.ESCAPE.isDown());
		Motor.A.stop();
		Motor.B.stop(true);
	}
	
	
	public static void testDirection() {
		Motor.A.stop();
		Motor.B.stop();
		
		Motor.A.rotate(40);						//flikt nach links und guckt ob dort eine Linie ist
		Motor.B.rotate(-40);
		if(sensor.getLightValue() < 44) {		//wenn ja wei� er dass er links lang muss
			direction = isLeft;				
		} else {								//wenn nein wei� er dass er rechts lang muss
			direction = isRight;		
		}	
		
		Motor.A.forward();
		Motor.B.forward();
	}
	
	public static void steerLeft() {			
		Motor.A.setSpeed(500); 					//erh�ht die geschwindigkeit um zu lenken (wird beim n�chsten loop zur�ckgesetzt)
		Motor.B.stop();
		
		do {}while(sensor.getLightValue() > 44);
	}
	
	public static void steerRight() {
		Motor.B.setSpeed(500); 					//erh�ht die geschwindigkeit um zu lenken (wird beim n�chsten loop zur�ckgesetzt)
		Motor.A.stop();
		
		do {}while(sensor.getLightValue() > 44);				//erh�ht die geschwindigkeit um zu lenken (wird beim n�chsten loop zur�ckgesetzt)
	}
	
}
