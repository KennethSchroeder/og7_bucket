package fastSeearch;

public class FastSearch {
	int[] zuDurchsuchen;
	
public FastSearch(int[] zuDurchsuchen) {
	this.zuDurchsuchen = zuDurchsuchen;
}
public long search(int von, int bis, int x) {
	long sucheFehlgeschlagen = -1;
	
	if(von <= bis) {
		int mb = (von+bis)/2;
		int mi = von+( (bis-von) * ( (x-zuDurchsuchen[von]) / (zuDurchsuchen[bis]-zuDurchsuchen[von]) ) );
		
		if (mb>mi) {
			int mn = mb;
			mb = mi;
			mi = mn;
		}
		if (x == zuDurchsuchen[mb]) {
			return mb;
		}
		if (x == zuDurchsuchen[mi]) {
			return mi;
		}
		if (x < zuDurchsuchen[mb]) {
			return search(von, mb-1, x);
		}
		if (x < zuDurchsuchen[mi]) {
			return search(von, mi-1, x);
		}
		else {
			return search(mi+1,bis,x);
		}
	}
	
	return sucheFehlgeschlagen;
}
}
