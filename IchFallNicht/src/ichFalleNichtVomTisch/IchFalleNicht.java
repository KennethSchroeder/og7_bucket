package ichFalleNichtVomTisch;

import lejos.nxt.Button;
import lejos.nxt.LightSensor;
import lejos.nxt.Motor;
import lejos.nxt.SensorPort;
import myException.boiFaelltException;

public class IchFalleNicht {

	public static void main(String[] args) throws boiFaelltException {
	
		
		faehrt();
			
	}
	
	public static void faehrt() {
		LightSensor sensorLeft = new LightSensor(SensorPort.S3);
		Motor.A.setSpeed(200);
		Motor.B.setSpeed(200);
		Motor.C.setSpeed(75);

		try {
			Motor.A.forward();
			Motor.B.forward();
			
			do {
				
				if(sensorLeft.getLightValue()<30) {
					throw new boiFaelltException()	;
				}
				Motor.C.rotate(45);			
				if(sensorLeft.getLightValue()<30) {
					throw new boiFaelltException()	;
				}
				Motor.C.rotate(45);
				
				if(sensorLeft.getLightValue()<30) {
					throw new boiFaelltException()	;
				}	
				Motor.C.rotate(-45);
				if(sensorLeft.getLightValue()<30) {
					throw new boiFaelltException()	;
				}	
				Motor.C.rotate(-45);
				
			}while(!Button.ENTER.isDown());
			
			}catch(Exception boiFaelltException) {
				
				Motor.A.backward();
				Motor.B.backward();
				Motor.C.stop();
				Motor.C.rotateTo(0);
				
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {}
				
				Motor.A.stop(true);
				Motor.B.stop();
			
				Motor.A.rotate(300);
				
				faehrt();
				
				}
	}
}
