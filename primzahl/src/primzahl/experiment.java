package primzahl;

public class experiment {
	private long[] primzahlen = { 10007, 10008, 20011, 55946, 20050, 30013, 25431, 50261, 50263, 2147483647 };
	primzahl myPrim = new primzahl();
	Zeit stoppuhr = new Zeit();	
	
	public experiment() {

	}
	
	public void durchlaufen() {
		for (int i = 0; i < primzahlen.length ;i++ ) {
			for (int s = 0; s<5; s++) {
				stoppuhr.start();
				boolean zahl = myPrim.isPrim(primzahlen[i]);
				long zeit = stoppuhr.stopp();
						
				System.out.println("Zahl: "+primzahlen[i]);
				System.out.println("Primzahl?: "+zahl);
				System.out.println("Benötigte Zeit: "+zeit);
				System.out.println();
			}
			System.out.println("------------------------------");
		}
	}
}
