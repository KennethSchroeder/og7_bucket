package zahlensortierer;

//TODO add your name as author
/**
 * @author "your name"
 *
 */

import java.util.Scanner;

public class Zahlensortierer {

	public static void main(String[] args) {

		Scanner myScanner = new Scanner(System.in);

		System.out.print("1. Zahl: ");
		int zahl1 = myScanner.nextInt();

		System.out.print("2. Zahl: ");
		int zahl2 = myScanner.nextInt();

		System.out.print("3. Zahl: ");
		int zahl3 = myScanner.nextInt();

		int[] zahlen = new int[3];
		zahlen[0] = zahl1;
		zahlen[1] = zahl2;
		zahlen[2] = zahl3;
		int kleinstes = zahlen[0];

		for (int i = 1; i < zahlen.length; i++) {
			if (kleinstes > zahlen[i]) {
				kleinstes = zahlen[i];
			}
		}
		System.out.println("Die kleinste Zahl ist: " + kleinstes);

		for (int i = 1; i < zahlen.length; i++) {
			if (kleinstes < zahlen[i]) {
				kleinstes = zahlen[i];
			}
		}
		System.out.println("Die groesste Zahl ist: " + kleinstes);

		myScanner.close();
	}
}
