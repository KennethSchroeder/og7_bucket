package keystore;

import java.util.Arrays;

public class KeyStore01 {
	private String[] keys;
	private int arrayPos = 0;
	private final int NICHT_GEFUNDEN = -1;
	
	
	public KeyStore01() {
		keys = new String[100];
	}
	
	public KeyStore01(int length) {
		keys = new String[length];
	}
	
	public boolean add(String e) {
		
		if (arrayPos<keys.length-1) {
			keys[arrayPos] = e;
			arrayPos++;
			return true;
		}
		else {
		return true;
		}
	}

	public void clear() {
		keys = new String[keys.length];
		arrayPos = 0;
	}
	
	public String get(int index) {
		return keys[index];
	}
	
	public int indexOf(String str) {
		for(int i = 0; i<keys.length; i++) {
			if (keys[i].equals(str)) {
				return i;
			}
		}
		return NICHT_GEFUNDEN;
	}
	
	public boolean remove(int index) {
		if (index>keys.length-1) {
			return false;
		}
		else {
			keys[index]=null;
			return true;
			}
	}

	public boolean remove(String str) {
		int s = indexOf(str);
		if(s == -1) {
			return false;
		}
		else {
			keys[s] = null;
			return true;
		}
	}

	public int size() {
		int menge = 0;
		for (int i = 0; i<keys.length; i++) {
			if (keys[i] != null) {
				menge++;
			}
		}
		return menge;
	}


	public String toString() {
		return "KeyStore01 [keys=" + Arrays.toString(keys) + "]";
	}

	
}

