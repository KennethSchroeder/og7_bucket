package keystore;

import java.util.ArrayList;
import java.util.Arrays;

public class KeyStore02 {
	private ArrayList<String> list = new ArrayList<String>();
	private int arrayPos = 0;
	private final int NICHT_GEFUNDEN = -1;
	
	
	public KeyStore02() {
	}
	
	public KeyStore02(int length) {
		
	}
	
	public boolean add(String e) {
	list.add(e);
	return true;
	}

	public void clear() {
		list.clear();
	}
	
	public String get(int index) {
		return list.get(index);
	}
	
	public int indexOf(String str) {
		return list.indexOf(str);
	}
	
	public boolean remove(int index) {
		list.remove(index);
		return true;
	}

	public boolean remove(String str) {
		int index = list.indexOf(str);
	    if (index > NICHT_GEFUNDEN) {
		list.remove(index);
		return true;
	    }
	    return false;
	    }

	public int size() {
		return list.size();
	}


	public String toString() {
		return "KeyStore02 [keys=" + list.toString() + "]";
	}

	
}

