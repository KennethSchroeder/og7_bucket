package buch;

import java.util.*;

public class CollectionTest {
	
	static void menue() {
		System.out.println("\n ***** Buch-Verwaltung *******");
		System.out.println(" 1) eintragen ");
		System.out.println(" 2) finden ");
		System.out.println(" 3) l�schen");
		System.out.println(" 4) Die gr��te ISBN");
		System.out.println(" 5) zeigen");
		System.out.println(" 9) Beenden");
		System.out.println(" ********************");
		System.out.print(" Bitte die Auswahl treffen: ");
	} // menue

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		List<Buch> buchliste = new LinkedList<Buch>();
        Scanner myScanner = new Scanner(System.in);

		char wahl;
		String eintrag;
		
		int index;
		do {
			menue();
			wahl = myScanner.next().charAt(0);
			switch (wahl) {
			case '1':	
                buchliste.add(erstelleBuch());
				break;
			case '2':
				System.out.println("Bitte geben sie die ISBN ein: ");
				String gesIsbn = ("ISBN-"+myScanner.next());
				Buch gefuBuch = findeBuch(buchliste,gesIsbn);
				System.out.println(gefuBuch.toString());
				break;
			case '3':
				System.out.println("Bitte die ISBN eingeben: ");
				String gesIsbn2 = ("ISBN-"+myScanner.next());
				System.out.println(loescheBuch(buchliste, gesIsbn2));
				break;
			case '4':
				System.out.println("Die gr��te ISBN ist: "+ermitteleGroessteISBN(buchliste));
				break;		
			case '5':
				System.out.println(buchliste.toString());
				break;
			case '9':
				System.exit(0);
				break;
			default:
				menue();
				wahl = myScanner.next().charAt(0);
		} // switch

	} while (wahl != 9);
}// main

	public static Buch findeBuch(List<Buch> buchliste, String isbn) {
		for (int i=0; i<buchliste.size();i++) {
			if (buchliste.get(i).getIsbn().equals(isbn)) {
				return buchliste.get(i);
			}
		}
		return null;
	}
	
	public static Buch erstelleBuch() {
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Bitte den Autor angeben: ");
		String autor = myScanner.next();
		System.out.println("Bitte den Titel angeben: ");
		String titel = myScanner.next();
		System.out.println("Bitte geben sie die ISBN an:");
		String isbn = "ISBN-"+myScanner.next();
		Buch buch = new Buch(autor,titel,isbn);
		
		return buch;
	}
	

	public static boolean loescheBuch(List<Buch> buchliste, String isbn) {
		for (int i=0; i<buchliste.size();i++) {
			if (buchliste.get(i).getIsbn().equals(isbn)) {
				buchliste.remove(i);
				return true;
			}
	}
		return false;
	}
	public static String ermitteleGroessteISBN(List<Buch> buchliste) {
		Buch f = buchliste.get(0);
		for (int i = 1; i<buchliste.size(); i++) {
			if(f.compareTo(buchliste.get(i))<0) { 
				f = buchliste.get(i);
			}
		}
		return f.getIsbn();		
		}
	}

