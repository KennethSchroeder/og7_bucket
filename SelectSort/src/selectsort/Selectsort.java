package selectsort;

public class Selectsort {

	
	public Selectsort() {
		
	}
	
	
	public long[] sort(long[] array) {
		
		
		for (int i = array.length-1; i >= 0; i--) {
			
			int maxPos = i;
			
			for(int m = maxPos-1; m >= 0 ; m--) {
				if (array[m]>array[maxPos]) {
					System.out.println(array[m] +" <-> "+array[maxPos]);
					long temp = array[maxPos];
					array[maxPos] = array[m];
					array[m] = temp;
				}
			}
		}
		
		return array;
	}
	
}
