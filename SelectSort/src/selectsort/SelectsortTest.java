package selectsort;

public class SelectsortTest {

	public static void main(String[] args) {
		Selectsort select = new Selectsort();
		
		long[] testarray = {1,84,6,5,78,95,23,22,45,1,10,56,11,12,36,65,98,75,41,20};
		
		System.out.print("Startanordnung: ");
		for (int i = 0; i<testarray.length; i++) {
			System.out.print(testarray[i]+", ");
		}
		System.out.println();
		//----------------------------------------------------
		long start = System.currentTimeMillis();
		long[] rueckgabe = select.sort(testarray);		//Algorithmusdurchlauf
		long ende = System.currentTimeMillis() - start;
		//----------------------------------------------------
		for (int i = 0; i<testarray.length; i++) {
			System.out.print(rueckgabe[i]+", ");
		}
		System.out.println("Ben. Zeit:"+ende+"ms");
	}

}
