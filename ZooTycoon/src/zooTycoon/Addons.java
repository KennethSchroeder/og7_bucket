package zooTycoon;

public class Addons {

	private int id;
	private String name;
	private double preis;
	private int stueckzahl;
	private String[] achievements = new String[3];
	
	public Addons (int id , String name , double preis) {
		this.id = id;
		this.name = name;
		this.preis = preis;
		this.stueckzahl = 0;
		this.achievements[0] = "keine";
	}
//---------------------------------------------------------------------------
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPreis() {
		return preis;
	}

	public void setPreis(double preis) {
		this.preis = preis;
	}

	public int getStueckzahl() {
		return stueckzahl;
	}

	public void setStueckzahl(int stueckzahl) {
		this.stueckzahl = stueckzahl;
	}

	public int getId() {
		return id;
	}

	public String[] getAchievements() {
		return achievements;
	}
	
	
//---------------------------------------------------------------------------
	
	public double kaufen() {
		stueckzahl++;
		System.out.println("1 mal "+name+"gekauft. Transaktion beendet");
		this.achievements();
		
		return 0.29;
	}
	
	private void achievements() {
		
		if (stueckzahl == 1) {
			achievements[0] = "erster Kauf!";
			System.out.println("----------Achievement erhalten: erster Kauf!----------");
		}
		if (stueckzahl == 5) {
			achievements[1] = "guter Kunde!";
			System.out.println("----------Achievement erhalten: guter Kunde!----------");
		}
		if (stueckzahl == 10) {
			achievements[2] = "Kaufrausch!";
			System.out.println("----------Achievement erhalten: Kaufrausch!----------");
		}
			
	}
	public void achievAnzeige() {
		System.out.println("Sie haben die Achievements: ");
		for (int i  = 0; i<3; i++) {
			System.out.println(achievements[i]);
		}
	}
	
}
