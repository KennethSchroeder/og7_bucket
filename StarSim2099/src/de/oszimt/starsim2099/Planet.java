package de.oszimt.starsim2099;

/**
 * Write a description of class Planet here.
 * 
 * @author (your name)
 * @version (a version number or a date)
 */
public class Planet extends PositionImRaum{

public int anzahlHafen;
public String name;
	
	// Methoden
//------------------------------------------------------------------
	public int getAnzahlHafen() {
	return anzahlHafen;
}

public void setAnzahlHafen(int anzahlHafen) {
	this.anzahlHafen = anzahlHafen;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public double getPosX() {
	return super.posX;
}

public void setPosX(double posX) {
	super.posX = posX;
}

public double getPosY() {
	return super.posY;
}

public void setPosY(double posY) {
	super.posY = posY;
}
//-----------------------------------------------------------------------------------------------------------
	// Darstellung
	public static char[][] getDarstellung() {
		char[][] planetShape = { { '\0', '/', '*', '*', '\\', '\0' }, { '|', '*', '*', '*', '*', '|' },
				{ '\0', '\\', '*', '*', '/', '\0' } };
		return planetShape;

	}
}
