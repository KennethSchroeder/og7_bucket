package mazeRunner;

import lejos.nxt.Button;

import lejos.nxt.LCD;
import lejos.nxt.LightSensor;
import lejos.nxt.Motor;
import lejos.nxt.SensorPort;
import lejos.nxt.Sound;

public class MazeRunner {

	public static void main(String[] args) {
		
		LightSensor sensorRight = new LightSensor(SensorPort.S2);
		LightSensor sensorLeft = new LightSensor(SensorPort.S3);
		
		/* Wert > 40 ist es wei�er Boden
		 * Wert > 30 ist es gr�nes Tape
		 * Wert > 20 ist es schwarzer Weg
		 */
		boolean stop = false;
		int countStop = 0; 
		Motor.A.setSpeed(150);
		Motor.B.setSpeed(150);				
		
		Button.waitForAnyPress();
		try {
		Thread.sleep(1000);
	} catch (InterruptedException ex) {};
		
		 do {
			 
			 LCD.drawInt(sensorLeft.getLightValue(), 1, 1);
			 LCD.drawInt(sensorRight.getLightValue(), 1, 2);
			 
			 Motor.A.forward();
			 Motor.B.forward();		
			 
			 
			 if(sensorLeft.getLightValue()>20 && sensorLeft.getLightValue()<30) {
				 flickLeft();
			 }
			 
			 if(sensorRight.getLightValue()>20 && sensorRight.getLightValue()<30) {
				 flickRight();
			 }
			 
			 if(sensorRight.getLightValue()>=30 && sensorRight.getLightValue()<40 || sensorLeft.getLightValue()>=30 && sensorLeft.getLightValue()<40) {
				 countStop++;
			 }
			 
			 
			 try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			 
			 
			if(Button.ESCAPE.isDown() || countStop >= 3) {
				stop = true;
			} 
			 
		 }while(stop != true);
		 Button.waitForAnyPress();
	}

	public static void flickRight() {
		
		Motor.A.stop();
		try {			
			Thread.sleep(200);
		} catch (InterruptedException e) {}
		Motor.A.forward();
		
	}
	
	public static void flickLeft() {
		
		Motor.B.backward();
		try {			
			Thread.sleep(200);
		} catch (InterruptedException e) {}
		Motor.B.forward();
	}
	
	public static void turn90() {
		Motor.B.stop();
		try {
			Thread.sleep(615);
		} catch (InterruptedException e) {}
		
		Motor.B.forward();
	}
	
}
