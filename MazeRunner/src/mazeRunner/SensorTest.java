package mazeRunner;

import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.nxt.LightSensor;
import lejos.nxt.SensorPort;

public class SensorTest {

	public static void main(String[] args) {
		
		LightSensor sensorRight = new LightSensor(SensorPort.S2);
		LightSensor sensorLeft = new LightSensor(SensorPort.S3);
		
		do {
			
			LCD.drawInt(sensorLeft.getLightValue(), 1, 1);
			LCD.drawInt(sensorRight.getLightValue(), 1, 2);
			LCD.drawInt(sensorLeft.getFloodlight(), 1, 3);
			LCD.drawInt(sensorLeft.getFloodlight(), 1, 4);
			
			
		}while(!Button.ENTER.isDown());


	}

}
