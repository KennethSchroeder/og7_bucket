package erstesProgramm;
import java.util.Random;

import lejos.addon.keyboard.KeyEvent;
import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.nxt.Motor;
import lejos.robotics.navigation.DifferentialPilot;

public class erstesProgramm {

	public static void main(String[] args) {
		
		int start = 1;
		
		do {
		start = showMenu();
		LCD.clear();
		switch (start) {
		
		case 1: 
			//schl�ft 3sek um ihn zu positionieren
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			//f�hrt 5sek gerade aus
				Motor.A.setSpeed(300);
				Motor.B.setSpeed(300);
				Motor.A.setAcceleration(1000);
				Motor.B.setAcceleration(1000);
				Motor.A.forward();
				Motor.B.forward();
				try {
					Thread.sleep(5000);
					} catch (InterruptedException e) {
				// TODO Auto-generated catch block
					e.printStackTrace();
					}
				Motor.A.stop(true);
				Motor.B.stop();
				
			//wartet um neue Sequenz zu verdeutlichen
			try {
				Thread.sleep(3000);
				} catch (InterruptedException e) {}
		
			//f�hrt 5sek zur�ck
						Motor.A.setSpeed(300);
						Motor.B.setSpeed(300);
						Motor.A.backward();
						Motor.B.backward();
						try {
						Thread.sleep(5000);
							} catch (InterruptedException e) {
					// TODO Auto-generated catch block
							e.printStackTrace();
						}
					Motor.A.stop(true);
						Motor.B.stop();
		break;
		case 2:
			//wartet um neue Sequenz zu verdeutlichen
			try {
			Thread.sleep(3000);
			} catch (InterruptedException e) {}
		
			
//f�hrt vorw�rts und dreht sich um 90 grad
	
		Motor.A.setSpeed(300);
		Motor.B.setSpeed(300);
		Motor.A.forward();
		Motor.B.forward();
		Motor.A.setAcceleration(6000);
	  Motor.B.setAcceleration(6000);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {}
		Motor.A.setSpeed(300);
		Motor.B.setSpeed(670);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {}
	
	Motor.A.stop(true);
	Motor.B.stop();
		break;
		case 3:
			//wartet um neue Sequenz zu verdeutlichen
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {}
			
			//Quadrat fahren
			
			Motor.A.setSpeed(300);
			Motor.B.setSpeed(300);
			Motor.A.forward();
			Motor.B.forward();
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {}
			
			Motor.B.backward();
			try {
				Thread.sleep(615);
			} catch (InterruptedException e) {}
			
			Motor.B.forward();
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {}
			
			Motor.B.backward();
			try {
				Thread.sleep(610);
			} catch (InterruptedException e) {}
			
			Motor.B.forward();
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {}
			
			Motor.B.backward();
			try {
				Thread.sleep(610);
			} catch (InterruptedException e) {}
			
			Motor.B.forward();
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {}
			
			Motor.B.backward();
			try {
				Thread.sleep(690);
			} catch (InterruptedException e) {}
			
			Motor.A.stop(true);
			Motor.B.stop();
		break;
		case 4:
			//wartet um neue Sequenz zu verdeutlichen
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {}
			
			//5 sek geradeaus
			
			Motor.A.setSpeed(300);
			Motor.B.setSpeed(300);
			Motor.A.forward();
			Motor.B.forward();

			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {}
			
			Motor.A.stop(true);
			Motor.B.stop();
		break;
		case 5:
			//wartet um neue Sequenz zu verdeutlichen
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {}
			
			//f�hrt einen Kreis
			Motor.A.setSpeed(300);
			Motor.B.setSpeed(600);
			Motor.A.forward();
			Motor.B.forward();
			
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {}
			
			Motor.A.stop(true);
			Motor.B.stop();
		break;
		case 6:
			//wartet um neue Sequenz zu verdeutlichen
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {}
			
			//f�hrt 1m
			
			Motor.A.setSpeed(300);
			Motor.B.setSpeed(300);
			Motor.A.forward();
			Motor.B.forward();

			try {
				Thread.sleep(7000);
			} catch (InterruptedException e) {}
			
			Motor.A.stop(true);
			Motor.B.stop();
			
			
		break;
		case 7:
			
			Motor.A.setSpeed(300);
			Motor.B.setSpeed(300);
			Motor.A.forward();
			Motor.B.forward();
			
			Random myRnd = new Random();
			int zufall = myRnd.nextInt(2);
			
			try {			
				Thread.sleep(7000);
			} catch (InterruptedException e) {}

			
			if(zufall==0) {
				Motor.B.backward();
				try {
					Thread.sleep(615);
				} catch (InterruptedException e) {}
				
				Motor.B.forward();
			}
			if(zufall==1) {
				Motor.A.backward();
				try {
					Thread.sleep(615);
				} catch (InterruptedException e) {}
				
				Motor.A.forward();
			}
			try {
				Thread.sleep(7000);
			} catch (InterruptedException e) {}
			Motor.A.stop(true);
			Motor.B.stop();
			
		break;
		default:
		}
		}while(start != -1);	
	}
		
		

	
	public static int showMenu() {
		
		LCD.drawString("fahren", 2, 1);
		LCD.drawString("Kurve", 2, 2);
		LCD.drawString("Quadrat", 2, 3);
		LCD.drawString("5sek", 2, 4);
		LCD.drawString("Kreis", 2, 5);
		LCD.drawString("Meter", 2, 6);
		LCD.drawString("Zufall", 2, 7);
		
		int wahl = 1;
		int wahlvor = 1;
		
		LCD.drawChar('*', 1, wahl);
		LCD.refresh();
		
		boolean start = false;
		do {
			
			Button.waitForAnyPress();
			
			if(Button.RIGHT.isDown()) {
				wahl++;
				wahlvor = wahl-1;
				if(wahl>8) {
					wahl = 1;
				}
			}
			if(Button.LEFT.isDown()) {
				wahl--;
				wahlvor = wahl+1;
				if(wahl<1) {
					wahl = 8;
				}
			}
		
			if(Button.ENTER.isDown()) {
				start = true;
			}
			
			if(Button.ESCAPE.isDown()) {
				return -1;
			}
			
			
			LCD.drawChar(' ', 1, wahlvor);
			LCD.drawChar('*', 1, wahl);
			LCD.refresh();
		}while(start != true);
		
		return wahl;
	}
	
	
}


