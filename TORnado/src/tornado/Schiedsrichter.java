package tornado;

public class Schiedsrichter extends Person {
	private int gepfiffeneSpiele;
	
	public Schiedsrichter (String name , int tel , boolean jahresbeitragBez, int gepfiffeneSpiele) {
		
		super( name ,  tel ,  jahresbeitragBez);
		this.gepfiffeneSpiele = gepfiffeneSpiele;
	}

	public int getGepfiffeneSpiele() {
		return gepfiffeneSpiele;
	}

	public void setGepfiffeneSpiele(int gepfiffeneSpiele) {
		this.gepfiffeneSpiele = gepfiffeneSpiele;
	}
	
}
