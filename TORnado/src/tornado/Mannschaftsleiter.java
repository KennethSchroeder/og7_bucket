package tornado;

public class Mannschaftsleiter extends Spieler {
	private String mannschaftsname;
	private int rabatt;
	
	public Mannschaftsleiter (String name , int tel , boolean jahresbeitragBez, int trikotnummer, String position, String mannschaftsname, int rabatt){
		super( name ,  tel ,  jahresbeitragBez,  trikotnummer,  position);
		this.mannschaftsname = mannschaftsname;
		this.rabatt = rabatt;
	}

	public String getMannschaftsname() {
		return mannschaftsname;
	}

	public void setMannschaftsname(String mannschaftsname) {
		this.mannschaftsname = mannschaftsname;
	}

	public int getRabatt() {
		return rabatt;
	}

	public void setRabatt(int rabatt) {
		this.rabatt = rabatt;
	}
	
}
