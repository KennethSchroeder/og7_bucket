package tornado;

public class Trainer extends Person{
	private char lizenzklasse;
	
	public Trainer (String name , int tel , boolean jahresbeitragBez, char lizenzklasse) {
		super( name ,  tel ,  jahresbeitragBez);
		this.lizenzklasse = lizenzklasse;
	}

	public char getLizenzklasse() {
		return lizenzklasse;
	}

	public void setLizenzklasse(char lizenzklasse) {
		this.lizenzklasse = lizenzklasse;
	}
	
	
}
