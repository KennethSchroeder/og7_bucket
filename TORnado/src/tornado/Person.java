package tornado;

public class Person {
	private String name ;
	private int tel;
	private boolean jahresbeitragBez;
	
//---------------------------------------------------------------------------
	
	public Person (String name , int tel , boolean jahresbeitragBez) {
		this.name = name;
		this.tel = tel;
		this.jahresbeitragBez = jahresbeitragBez;
	}
	
//---------------------------------------------------------------------------
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getTel() {
		return tel;
	}
	public void setTel(int tel) {
		this.tel = tel;
	}
	public boolean isJahresbeitragBez() {
		return jahresbeitragBez;
	}
	public void setJahresbeitragBez(boolean jahresbeitragBez) {
		this.jahresbeitragBez = jahresbeitragBez;
	}
	
}
