package tornado;

public class Spieler extends Person{
	private int trikotnummer;
	private String position;
	
	
	public Spieler (String name , int tel , boolean jahresbeitragBez, int trikotnummer, String position) {
		super ( name ,  tel ,  jahresbeitragBez);
		this.trikotnummer = trikotnummer;
		this.position = position;
		
		
	}


	public int getTrikotnummer() {
		return trikotnummer;
	}


	public void setTrikotnummer(int trikotnummer) {
		this.trikotnummer = trikotnummer;
	}


	public String getPosition() {
		return position;
	}


	public void setPosition(String position) {
		this.position = position;
	}
}
