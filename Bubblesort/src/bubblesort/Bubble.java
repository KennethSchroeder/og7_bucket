package bubblesort;

public class Bubble {

	public Bubble() {
		
	}
	
	public long[] sortieren (long[] zahlen) {
		
		for (int i = 0; i<zahlen.length-1; i++) {
			for (int j = 0; j<zahlen.length-1-i; j++) {	  
				
				if (zahlen[j] > zahlen[j+1]) {
					long zwischen = zahlen[j];
					zahlen[j] = zahlen[j+1];
					zahlen[j+1] = zwischen;
					
				}
				
				}
			}
		return zahlen;
	}

}
